//+------------------------------------------------------------------+
//|                                                                  |
//|                           Copyright 2016, Sirisak Jaroonrojwong. |
//|                                          http://www.sirisakj.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <meff_struct.mqh>
#include <meff_helper.mqh>
#include <meff_noti.mqh>
#include <meff_core1.mqh>




InfoOrder orderList[21];
input int sizeOrder = 21; // Initial size of Order max
input string orderSymblo01 = "EURUSD"; //Initial Symblo Order 1
input string orderSymblo02 = "USDJPY"; //Initial Symblo Order 2
input string orderSymblo03 = "GBPUSD"; //Initial Symblo Order 3
input string orderSymblo04 = "USDCHF"; //Initial Symblo Order 4
input string orderSymblo05 = "USDCAD"; //Initial Symblo Order 5
input string orderSymblo06 = "AUDUSD"; //Initial Symblo Order 6
input string orderSymblo07 = "GBPJPY"; //Initial Symblo Order 7
input string orderSymblo08 = "EURCAD"; //Initial Symblo Order 8
input string orderSymblo09 = "GBPCHF"; //Initial Symblo Order 9

input string orderSymblo10 = "AUDUAD"; //Initial Symblo Order 10
input string orderSymblo11 = "EURGBP"; //Initial Symblo Order 11
input string orderSymblo12 = "EURAUD"; //Initial Symblo Order 12
input string orderSymblo13 = "EURCHF"; //Initial Symblo Order 13
input string orderSymblo14 = "EURJPY"; //Initial Symblo Order 14
input string orderSymblo15 = "CADJPY"; //Initial Symblo Order 15
input string orderSymblo16 = "AUDCAD"; //Initial Symblo Order 16
input string orderSymblo17 = "AUDCHF"; //Initial Symblo Order 17
input string orderSymblo18 = "CADCHF"; //Initial Symblo Order 18
input string orderSymblo19 = "XAUUSD"; //Initial Symblo Order 19
input string orderSymblo20 = "GBPAUD"; //Initial Symblo Order 20
input string orderSymblo21 = "GBPCAD"; //Initial Symblo Order 21


input int dayReset = 4; // Days for reset robot






bool isAutoTrade = false;
bool isAllowNoti = true;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+


int OnInit()
  {
   
   if(TimeCurrent() > nextTime){
      nextTime = TimeCurrent() + 10 + ((MathRand()%1440)*60);
   }
  
   
   
   if(sizeOrder >= 1) initInfoOrder(orderSymblo01, orderList[0]);  
   if(sizeOrder >= 2) initInfoOrder(orderSymblo02, orderList[1]);  
   if(sizeOrder >= 3) initInfoOrder(orderSymblo03, orderList[2]);  
   if(sizeOrder >= 4) initInfoOrder(orderSymblo04, orderList[3]);  
   if(sizeOrder >= 5) initInfoOrder(orderSymblo05, orderList[4]);  
   if(sizeOrder >= 6) initInfoOrder(orderSymblo06, orderList[5]);  
   if(sizeOrder >= 7) initInfoOrder(orderSymblo07, orderList[6]);  
   if(sizeOrder >= 8) initInfoOrder(orderSymblo08, orderList[7]);  
   if(sizeOrder >= 9) initInfoOrder(orderSymblo09, orderList[8]); 
   if(sizeOrder >= 10) initInfoOrder(orderSymblo10, orderList[9]); 
   if(sizeOrder >= 11) initInfoOrder(orderSymblo11, orderList[10]); 
   if(sizeOrder >= 12) initInfoOrder(orderSymblo12, orderList[11]); 
   if(sizeOrder >= 13) initInfoOrder(orderSymblo13, orderList[12]); 
   if(sizeOrder >= 14) initInfoOrder(orderSymblo14, orderList[13]); 
   if(sizeOrder >= 15) initInfoOrder(orderSymblo15, orderList[14]); 
   if(sizeOrder >= 16) initInfoOrder(orderSymblo16, orderList[15]); 
   if(sizeOrder >= 17) initInfoOrder(orderSymblo17, orderList[16]); 
   if(sizeOrder >= 18) initInfoOrder(orderSymblo18, orderList[17]); 
   if(sizeOrder >= 19) initInfoOrder(orderSymblo19, orderList[18]); 
   if(sizeOrder >= 20) initInfoOrder(orderSymblo20, orderList[19]); 
   if(sizeOrder >= 21) initInfoOrder(orderSymblo21, orderList[20]); 
   
   
   
   
   
   SendMail(StringFormat("Start %s", TimeToStr(startTime)),"");
   
   for(int i=0; i<sizeOrder;i++){
         Print(orderList[i].symblo);
   }
      
      
   refreshView();
//--- create timer
   EventSetTimer(1);

    
    
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
  
  
  SendMail(StringFormat("Stop %s", TimeToStr(TimeCurrent())),"");
//--- destroy timer

   EventKillTimer();
      
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+




void OnTick()
  {


   int _line = 0;
   
   if(statusId != 1)
   {
      return;
   }
   
   loop++;
   if(loop < 0){
      loop = 0;
      warnCount++;
   }
   
   for(int i=0; i<sizeOrder;i++){
      
      //orderList[i].;
      // Stochastic Process
      OrderBill orderBill;
      executeCore1(orderList[i],orderBill);
      
   }
   
   isFirstTime = false;
 
   
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int statusId = 1;
int saveStatusId = 0;
string status = "";
long errorCount = 0;
string error = "";
int warnCount = 0;
long loop = 0;

datetime startTime = TimeCurrent();
datetime nextTime= D'2017.04.01 00:00';

void checkAllow(){
  if(TimeCurrent() > nextTime){
      statusId = 2;
      error = "Array out of index";
   }
   else
   {
      error = "";
      statusId = 1;
   }
   
   
}
void OnTimer()
  {
//---
      //nextime = nextime - (nextime % (1*60*60*24));
      //nextime = nextime + 1*60*60*24;
      checkAllow();
      switch(statusId){
         case 1 :status = "RUN"; break;
         case 2 :status = "BREAK"; break;
         case 0 :
         default : status = "STOP"; break;
      }
      
      OutText(StringFormat("STATUS:%s", status),0);
      OutText(StringFormat("%s", error),1);
      OutText(StringFormat("WARN C:%d", warnCount),2);
      
      OutText(StringFormat("AT:%s", TimeToStr(startTime)),3);
      //OutText(StringFormat("ED:%s", TimeToStr(nextTime)),4);
      OutText(StringFormat("L:%d", loop),4);
      OutText(StringFormat("TF=%d(sec)", PeriodSeconds()),5);
     // OutText(StringFormat("AUTO_BOT=%s", isAutoTrade ? "TRUE":"FALSE"),6);
     
     
 
  }
  
void refreshView (){
   OutButton(StringFormat("BOT %s", isAutoTrade ? "AUTO":"MANUAL"),140,15,100,30,"btnBot");
   
   
   
}


/*
double sto_test1_main []  = {70,81,83,90,85,60};
double sto_test1_signal[] = {60,70,81,81,90,65};
//*/



double sto_test1_main []  = {41.7777f,23.6040f,9.4128f,6.5727f,7.5476f};
double sto_test1_signal[] = {29.6313f,17.4849f,11.3659f,13.3190f,20.0652f};

int sto_test1_size = 5;






//####################################################################
//PRINT TEXT
//####################################################################
void OutText(string text, int line){
     
   OutText(text,line,0xFFFFFF );
   
}
void OutText(string text, int line,color c){
 string name =  StringConcatenate("strLine", IntegerToString(line));
     
     if(ObjectFind(name) == -1){
         ObjectCreate(name,OBJ_BUTTON,0,0,0);
     }
      int bgcolor = 0x000000;
   
   ObjectSetInteger(0,name,OBJPROP_XDISTANCE,5);
   ObjectSetInteger(0,name,OBJPROP_YDISTANCE,15 + (line*19));
   ObjectSetInteger(0,name,OBJPROP_BGCOLOR,bgcolor);
    ObjectSetInteger(0,name, OBJPROP_BACK, false);
   ObjectSetInteger(0,name, OBJPROP_COLOR, 0xffffff);
   ObjectSetString(0,name,OBJPROP_TEXT,text);
   ObjectSetInteger(0,name,OBJPROP_XSIZE,130);
ObjectSetInteger(0,name,OBJPROP_YSIZE,20);
ObjectSetInteger(0,name,OBJPROP_FONTSIZE,8); 

   ObjectSetInteger(0,name,OBJPROP_BORDER_COLOR,bgcolor);
ObjectSetInteger(0,name,OBJPROP_BORDER_TYPE,BORDER_FLAT);
   //ObjectSetInteger(0,name,OBJPROP_ALIGN  ,BORDER_FLAT);
   
   /*
   
   //ObjectSetText(name,text,10,"Arial",c );
   string name =  StringConcatenate("strLine", IntegerToString(line));
     
     if(ObjectFind(name) == -1){
         ObjectCreate(name,OBJ_RE,0,0,0);
     }
         
   
   ObjectSet(name,OBJPROP_XDISTANCE,5);
   ObjectSet(name,OBJPROP_YDISTANCE,60 + (line*12));
   ObjectSet(name,OBJPROP_BGCOLOR,0x000000);
    ObjectSet(name, OBJPROP_BACK, false);
   ObjectSet(name, OBJPROP_COLOR, c);
   
   ObjectSetText(name,text,10,"Arial",c );
   
   //*/
}

void OutButton(string text,int x1,int y1,int w,int h, string name, color c = clrBlack, color bg = clrWhite){


     if(ObjectFind(name) == -1){
         ObjectCreate(name,OBJ_BUTTON,0,0,0);
     }
   
   ObjectSetInteger(0,name,OBJPROP_XDISTANCE,x1);
   ObjectSetInteger(0,name,OBJPROP_YDISTANCE,y1);
   ObjectSetInteger(0,name,OBJPROP_BGCOLOR,bg);
   // ObjectSetInteger(0,name, OBJPROP_BACK, false);
   ObjectSetInteger(0,name, OBJPROP_COLOR, c);
   ObjectSetString(0,name,OBJPROP_TEXT,text);
   ObjectSetInteger(0,name,OBJPROP_XSIZE,w);
   ObjectSetInteger(0,name,OBJPROP_YSIZE,h);
   ObjectSetInteger(0,name,OBJPROP_STATE,false);
   ObjectSetInteger(0,name,OBJPROP_FONTSIZE,8); 
   //ObjectSetInteger(0,name,OBJPROP_BORDER_COLOR,bgcolor);
   ObjectSetInteger(0,name,OBJPROP_BORDER_TYPE,BORDER_FLAT);
  
}
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---
   if(id==CHARTEVENT_OBJECT_CLICK)
   {
     if(sparam=="btnBot")
         {
            isAutoTrade = !isAutoTrade;
            
            refreshView();
         
         }
    }
}

void PrintLog(){
   MqlTick last_tick;
//---
   if(SymbolInfoTick(Symbol(),last_tick))
   {
      int i = 2; //Time[i], Open[i], High[i], Low[i], Close[i], Volume[i]); 

      OutText(StringFormat("- Bid %f",last_tick.bid),i++);
      OutText(StringFormat("- Ask %f",last_tick.ask),i++);
      OutText(StringFormat("- Volume %d",Volume[0]),i++);
      OutText(StringFormat("- Open %f",Open[0]),i++);
      OutText(StringFormat("- Close %f",Close[0]),i++);
      OutText(StringFormat("- High %f",High[0]),i++);
      OutText(StringFormat("- Low %f",Low[0]),i++);
      OutText(StringFormat("- Time %s",TimeToStr(Time[0])),i++);
      OutText(StringFormat("- Time Type %s",GetTimeType()),i++,clrYellow);
   }
   else
   {
      //Order
      Print("SymbolInfoTick() failed, error = ",GetLastError());
   }
}




//####################################################################
//POINT
//####################################################################
// cmd = OP_BUY or OP_SELL, 
void excuteOrder(int cmd, double lots, double price , double takeprofit,InfoOrder& order) 
  { 
//--- get minimum stop level 
   double minstoplevel=MarketInfo(order.symblo,MODE_STOPLEVEL); 
   

//--- calculated SL and TP prices must be normalized 
  double stoploss=NormalizeDouble(Bid-minstoplevel*Point,Digits); 
  // double takeprofit=NormalizeDouble(Bid+minstoplevel*Point,Digits); 
//--- place market order to buy 1 lot 
   int slippage = 1;
   int ticket=OrderSend(order.symblo,cmd,lots,price,slippage,0,takeprofit,NULL,0,0,clrGreen); 
   if(ticket<0) 
     { 
      SendMail("[ERR]", StringFormat("OrderSend failed with error # %d",GetLastError()));
      Print("OrderSend failed with error #",GetLastError()); 
     } 
   else 
      Print("OrderSend placed successfully"); 
//--- 
  }
  