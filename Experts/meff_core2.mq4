//+------------------------------------------------------------------+
//|                                                   meff_core3.mq4 |
//|                                                        Sirisak.J |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Sirisak.J"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

const int ID_ORDER = 0;
const int ID_TP = 1;
const int ID_SL = 2;
const int ID_TPK = 3;
const int ID_SLK = 4;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   double orderPrice = iCustom(NULL,0,"meff_core3_in",ID_ORDER,0);
   double slPrice = iCustom(NULL,0,"meff_core3_in",ID_SL,0);
   double tpPrice = iCustom(NULL,0,"meff_core3_in",ID_TPK,0);
   
   Print(orderPrice,":", slPrice,":", tpPrice);
   
  }
//+------------------------------------------------------------------+
