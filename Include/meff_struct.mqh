//+------------------------------------------------------------------+
//|                                                                  |
//|                           Copyright 2016, Sirisak Jaroonrojwong. |
//|                                          http://www.sirisakj.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <meff_struct.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
struct InfoOrder {
   string symblo;
   bool isNeedStochastic;
   int stoSmallDirection;
   double currentStoIntersectSmall;
   
   int lastStoSmallDirection;
   datetime lastStoSmallTimeSave;
   datetime currentStoSmallTimeSave;
   
   int lastCCIDirection;
   datetime lastCCITimeSave;
   datetime currentCCITimeSave;
   
   int timeTest;
   
   int lastDirectionForAllowNoti;
   datetime lastTimeForAllowNoti;
   
   double lastStoIntersectSmall;
   
   double lastCCI;
   double lastBBUpper;
   double lastBBLower;
   double lastBBMain;

};

struct FPoint {
   double x;
   double y;
};

struct OrderBill {
   bool isValid;
   int cmd; 
   double lots; 
   double price; 
   double takeprofit;
   string symblo;

};

 void initInfoOrder(string symblo,InfoOrder& order){

   order.symblo = symblo;
   order.isNeedStochastic = true;
   order.stoSmallDirection = 0;
   order.currentStoIntersectSmall = -1;
   
   order. lastStoSmallDirection = 0;
   order. lastStoSmallTimeSave = Time[10];
   order. currentStoSmallTimeSave = Time[0];
   
   order. lastCCIDirection = 0;
   order. lastCCITimeSave = Time[10];
   order. currentCCITimeSave = Time[0];
   
   order. timeTest = 0;
   
   order. lastDirectionForAllowNoti = 0;
   order. lastTimeForAllowNoti =  Time[0];
   
   order. lastStoIntersectSmall= -1;
   
   order. lastCCI = -1;
   order. lastBBUpper = 0;
   order. lastBBLower = 0;
   order. lastBBMain = 0;
   
}