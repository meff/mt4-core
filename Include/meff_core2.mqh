//+------------------------------------------------------------------+
//|                                                                  |
//|                           Copyright 2016, Sirisak Jaroonrojwong. |
//|                                          http://www.sirisakj.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict


#include <meff_struct.mqh>
#include <meff_helper.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool isFirstTime=false;

void executeCore2(InfoOrder& order, OrderBill& orderBill){
   
   orderBill.isValid = false;
   
   iCustom(NULL,PERIOD_M15,"Dolly Graphics v18",timeframe,mode,shift);
   
}

