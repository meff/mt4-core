//+------------------------------------------------------------------+
//|                                                  meff_helper.mqh |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <meff_struct.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
bool hasOrder(InfoOrder& order){
    
  int total=OrdersTotal();
  // write open orders
  for(int pos=0;pos<total;pos++)
    {
     if(OrderSelect(pos,SELECT_BY_POS)==false) continue;
     
     string orderName = OrderSymbol();
     //PrintFormat("Symblo : %s, %s",order.symblo,orderName,); 

     if(StringCompare(order.symblo,orderName,false) == 0){
         return true;    
     }     
    }
    
   return false;
}

datetime getTimeAt(int bar){
   return Time[bar];
}

// return in hour value
double GetDiffTime(){
   double diff = (Time[0]-Time[1])/3600.0;
   return diff;
}

string GetTimeType(){
   double diff = GetDiffTime();
   
   if(diff >= 744.0){
      diff = diff / 744.0;
      return StringFormat("MN%0.0f",diff);
   }
   if(diff >= 168.0){
      diff = diff / 168.0;
      return StringFormat("W%0.0f",diff);
   }
   if(diff >= 24.0){
      diff = diff / 24.0;
      return StringFormat("D%0.0f",diff);
   }
   if(diff >= 1.0){
      return StringFormat("H%0.0f",diff);
   }
   diff = diff * 60;
   return StringFormat("M%0.0f",diff);  
   
}

//####################################################################
//TOUCH
//####################################################################

bool isTouch( int direction, double currentValue,double prevValue, double touchValue){

   if( direction > 0 ){
      return prevValue < touchValue && touchValue < currentValue;
   }   
   return prevValue > touchValue && touchValue > currentValue;   
}

int isDirection(double prevValue, double currentValue){
   int direction = 0;
   if( prevValue > currentValue ){
      return -1;
   }   
   else if( prevValue < currentValue )
   {
    return +1;
   }
   
   return direction;   
}



bool isArea( int direction, double currentValue, double areaValue){

   if( direction > 0 ){
      return areaValue < currentValue;
   }   
   return areaValue > currentValue;   
}

   double getSlope(FPoint &p1, FPoint &p2){
   return -1*(p2.y-p1.y)/(p2.x-p1.x);
}

//####################################################################
//POINT
//####################################################################

int orientation(FPoint &p, FPoint &q, FPoint &r) {
    double val = (q.y - p.y) * (r.x - q.x)
            - (q.x - p.x) * (r.y - q.y);

    if (val == 0.0)
        return 0; // colinear
    return (val > 0) ? 1 : 2; // clock or counterclock wise
}

bool intersect(FPoint &p1, FPoint &q1, FPoint &p2, FPoint &q2) {

    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    if (o1 != o2 && o3 != o4)
        return true;

    return false;
}

FPoint intersectAt(FPoint &p1, FPoint &q1, FPoint &p2, FPoint &q2) {

        double x1,y1, x2,y2, x3,y3, x4,y4;
        double x,y;
        FPoint res = {0,0};
        x1 = p1.x; 
        y1 = p1.y; 
        x2 = q1.x; 
        y2 = q1.y;
        x3 = p2.x; 
        y3 = p2.y; 
        x4 = q2.x; 
        y4 = q2.y;
        
        x = (
                (x2 - x1)*(x3*y4 - x4*y3) - (x4 - x3)*(x1*y2 - x2*y1)
                ) /
                (
                (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );
        y = (
                (y3 - y4)*(x1*y2 - x2*y1) - (y1 - y2)*(x3*y4 - x4*y3)
                ) /
                (
                (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4)
                );
                
        res.x = x;
        res.y = y;
        
        return res ;

    }


//####################################################################
//Time
//####################################################################

double getDayDistance(datetime from, datetime to){
   return (to - from) / (86400.0);
}

