//+------------------------------------------------------------------+
//|                                                                  |
//|                           Copyright 2016, Sirisak Jaroonrojwong. |
//|                                          http://www.sirisakj.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
input bool sto_enable = true; // Stochastic Enable to use
input int sto_skipBars = 0; // Stochastic skip bar 0 - n

input int stoS_LineHigh = 80; // Stochastic Short High-Line
input int stoS_LineLow = 20; // Stochastic Short Low-Line
input int stoS_Kperiod = 5; // Stochastic Short %K period
input int stoS_Dperiod = 3; // Stochastic Short %D period
input int stoS_Slowing = 3; // Stochastic Short Slowing

input int stoL_LineHigh = 80; // Stochastic Long High-Line
input int stoL_LineLow = 20; // Stochastic Long Low-Line
input int stoL_Kperiod = 14;// Stochastic Long %K period
input int stoL_Dperiod = 3; // Stochastic Long %D period
input int stoL_Slowing = 5; // Stochastic Long Slowing

input int cci_LineHigh = 100; // CCI High-Line
input int cci_LineLow = -100; // CCI Low-Line
input int cci_period = 14; // CCI period
input int cci_skipBars = 0; // CCI skip bar 0 - n


input double bb_percentOrder = 0.40; // % Order of BB
input double bb_percentTP = 0.85; // % TP of BB
//input double bb_percent_diff = 0.05;     // Bollinger Percent match value
input double bb_pipNeary = 30;  // Bollinger PIP value for check neary indicatior
input int bb_period = 20;     // Bollinger Bands period
input int bb_deviation = 2;   // Bollinger Bands standard deviations
input int bb_skipBars = 1;    // Bollinger Bands skip bar 0 - n

#include <meff_struct.mqh>
#include <meff_helper.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool isFirstTime=false;

void executeCore1(InfoOrder& order, OrderBill& orderBill){
   
   orderBill.isValid = false;
   bool isStoAlert = checkStochasticCorrect(sto_skipBars,Bars-1,order);
      if(isStoAlert){
         
         order.currentStoSmallTimeSave = getTimeAt(1);
         if(order.lastStoSmallTimeSave != order.currentStoSmallTimeSave){
              //Print("Stochastic Process");
            //OutText(StringFormat("last intersect Sto(5) %f", lastStoIntersectSmall),18);
            //OutText(StringFormat("last Time %s",TimeToStr(Time[0])),19);
            order.currentStoIntersectSmall = order.lastStoIntersectSmall;
            order.lastStoSmallTimeSave = order.currentStoSmallTimeSave;
            
            string action = "";
            double value = 0;
            int direction = getLastStochasticDirection(sto_skipBars,order);
            if(direction == -1){
               order.stoSmallDirection = -1;
            }
            else if(direction == +1){
               order.stoSmallDirection = +1;
            }
            else {
               order.stoSmallDirection = 0;
            }
            
            order.lastStoSmallDirection = order.stoSmallDirection;
            
           
         }
   
         //
      }
      
   
      
      // CCI Process
      int cciDirection = checkCCIDirection(cci_skipBars, Bars-1,order);
      if( cciDirection !=0 ){
         
         order.currentCCITimeSave = getTimeAt(1);
         
         if(order.lastCCITimeSave != order.currentCCITimeSave)
         {
            //Print("CCI Process");
            order.lastCCITimeSave = order.currentCCITimeSave;
           
            
            order.lastCCIDirection = cciDirection;
         }
      }
      
      // BB Process
      bool isAllowNoti = false;
      if(order.lastCCIDirection != 0 && order.lastStoSmallDirection != 0 && order.lastCCIDirection == order.lastStoSmallDirection){
      
         //Print("BB Process");
         
         int direction = order.lastCCIDirection;
         
         double pip = getRealPIPSpan(order);
         double lowValue = getBBLower(bb_skipBars,order);
         double cenValue = getBBMain(bb_skipBars,order);
         double highValue = getBBUpper(bb_skipBars,order);
         
         double dZoneUpper = (highValue - High[bb_skipBars]);
         
          double dZoneLower = (Low[bb_skipBars] - lowValue);
          
       
          
         if(direction == -1 && dZoneUpper <= pip){
               isAllowNoti = true;
            }
            else if(direction == +1 && dZoneLower  <= pip ){
               isAllowNoti = true; 
            }
          
          isAllowNoti = true;
      }
      
      // Last Process for Notification
      double distanceDayForResetBot = getDayDistance(order.lastTimeForAllowNoti,Time[0]);
          
      if( !hasOrder(order) && isAllowNoti && (order.lastDirectionForAllowNoti != order.lastCCIDirection || distanceDayForResetBot > 4 ) && order.lastCCIDirection == getLastStochasticLargeDirection(sto_skipBars,order)){
         
         int direction = order.lastCCIDirection;
         order.lastDirectionForAllowNoti = order.lastCCIDirection;
         order.lastTimeForAllowNoti = Time[0];
         
         
         string action = "";
         double value = 0;
         int cmd = 0;
         double lots = 0.01;
         
         double distance = 10;
         
         double lowValue   = getBBLower(bb_skipBars,order);
         double cenValue   = getBBMain(bb_skipBars,order);
         double highValue  = getBBUpper(bb_skipBars,order);
         double tp = 0;
         bool allowOrder = false;
            if(direction == -1 || isFirstTime){
               action = "Sell";
               value = MarketInfo(order.symblo,MODE_BID); 

               cmd = OP_SELL;
               tp = ((highValue - cenValue) * (1.0 - bb_percentTP)) + cenValue;
               //tp = value - distance*Point;
               //Print(StringFormat("TP SELL####### %f",tp));
               //Print(StringFormat("TP SELL####### %f",(highValue-value)/(highValue - cenValue) ));
               allowOrder =((highValue-value)/(highValue - cenValue))<= bb_percentOrder;
               
            }
            else if(direction == +1 || isFirstTime){
               action = "Buy";
               value =  MarketInfo(order.symblo,MODE_ASK);
               cmd = OP_BUY;
                tp = ((cenValue - lowValue ) * bb_percentTP) + lowValue;
               // Print(StringFormat("TP BUY####### %f",tp));
               
               //Print(StringFormat("TP SELL####### %f",(value - lowValue)/ (cenValue - lowValue) ));
                allowOrder = (value - lowValue)/ (cenValue - lowValue) <= bb_percentOrder;
              
            }
            else {
               action = "Mid";
               value = (MarketInfo(order.symblo,MODE_BID)+MarketInfo(order.symblo,MODE_ASK))/2.0f;
              
            }
         
         if(isFirstTime || allowOrder){
            //sendNoti(isAutoTrade, action, value,order);
            orderBill.cmd = cmd;
            orderBill.lots = lots;
            orderBill.price = value;
            orderBill.takeprofit = tp;
            
            orderBill.symblo = order.symblo;
            orderBill.isValid = true;
            //if(isAutoTrade){
             //  excuteOrder(cmd, lots, value, tp,order); 
            //}
            
           // string output = StringConcatenate (StringFormat("%s,",order.symblo),"Signal|","CCI=",StringFormat("%.5f",getCCI(0,order)),"|STO(5)=",StringFormat("%.5f",getStochasticSmall(true,0,order)) ,"|",action,"=",value);
           // Print(output);
         }
         
         
         if(isFirstTime || isAllowNoti){
           // order.lastCCIDirection = order.lastStoSmallDirection = 0;
            
           
         }
         
      }
      
   //return orderBill;
}


//####################################################################
//Stochastic
//####################################################################
double getStochastic(int Kperiod, int Dperiod,int slowing, bool isMain, int barAt,InfoOrder& order){
   int timeframe = 0; // 0 means the current chart timeframe.
   int pricefield = 0; // 0 - Low/High or 1 - Close/Close.
   
   //double sto[] = isMain ? sto_test1_main : sto_test1_signal;
   
   /*
   if(isMain)
      return sto_test1_main[barAt];
      
   return sto_test1_signal[barAt];
   //*/
   
   return iStochastic(order.symblo,timeframe,Kperiod,Dperiod,slowing,MODE_EMA,pricefield,isMain ? MODE_MAIN : MODE_SIGNAL,barAt);
}

double getStochasticSmall( bool isMain,int barAt,InfoOrder& order){
   return getStochastic(stoS_Kperiod,stoS_Dperiod,stoS_Slowing,isMain,barAt,order);
}

double getStochasticLarge( bool isMain,int barAt,InfoOrder& order){
   return getStochastic(stoL_Kperiod,stoL_Dperiod,stoL_Slowing,isMain,barAt,order);
}



int getLastStochasticDirection(int barAt,InfoOrder& order){

      const int i = barAt;
      FPoint pS1;
      FPoint qS1;
     
      
      pS1.x = i;
      pS1.y = getStochasticSmall(true,i,order);
      qS1.x = i+1;
      qS1.y = getStochasticSmall(true,i+1,order);
      
      bool isAboveS1Touch = isTouch(-1, pS1.y, qS1.y, stoS_LineHigh);
      bool isBelowS1Touch = isTouch(+1, pS1.y, qS1.y, stoS_LineLow);
      
      if(isAboveS1Touch){
         return -1;
      }
      if(isBelowS1Touch){
         return +1;
      }
      
      return 0;
}

int getLastStochasticLargeDirection(int barAt,InfoOrder &order){

      const int i = barAt;
      int direction = 0;
      FPoint pS1;
      FPoint qS1;
      FPoint pS2;
      FPoint qS2;
     
      
      pS1.x = i;
      pS1.y = getStochasticLarge(true,i,order);
      qS1.x = i+1;
      qS1.y = getStochasticLarge(true,i+1,order);
      
      pS2.x = i;
      pS2.y = getStochasticLarge(false,i,order);
      qS2.x = i+1;
      qS2.y = getStochasticLarge(false,i+1,order);
      
      if(( isArea(+1, pS1.y, stoL_LineHigh) || isArea(+1, qS1.y, stoL_LineHigh) ) ||( isArea(-1, pS1.y, stoL_LineLow) || isArea(-1, qS1.y, stoL_LineLow) ))
      {
           
            return isDirection( qS1.y , pS1.y);
      }
      
      
     
      
      return 0;
}



bool checkStochasticCorrect(int barAt, int sizeBar,InfoOrder& order){
   bool isIntersect = false;
   double isIntersectAt = -1;
  
   bool isStoSmallMainTouched = false;
   
   for( int j = barAt; j < sizeBar; j++){
      const int i = j;
      FPoint pS1;
      FPoint qS1;
      FPoint pS2;
      FPoint qS2;
      //FPoint pLarge = {(double)i, getStochasticLarge(true,i)};      
      //FPoint qLarge = {(double)(i+1), getStochasticLarge(false,i)};
      
      pS1.x = i;
      pS1.y = getStochasticSmall(true,i,order);
      qS1.x = i+1;
      qS1.y = getStochasticSmall(true,i+1,order);
      
      pS2.x = i;
      pS2.y = getStochasticSmall(false,i,order);
      qS2.x = i+1;
      qS2.y = getStochasticSmall(false,i+1,order);
      
      
      
      if(( isArea(+1, pS1.y, stoS_LineHigh) || isArea(+1, qS1.y, stoS_LineHigh) ) ||( isArea(-1, pS1.y, stoS_LineLow) || isArea(-1, qS1.y, stoS_LineLow) ))
      {
      
         bool isAboveS1Touch = isTouch(-1, pS1.y, qS1.y, stoS_LineHigh);
         bool isBelowS1Touch = isTouch(+1, pS1.y, qS1.y, stoS_LineLow);
         if(!isStoSmallMainTouched && ( isAboveS1Touch || isBelowS1Touch )){
            
            double slope = getSlope(pS2,qS2);
            
            //OutText(StringFormat("last Time %f",slope),20);
            
            if(isAboveS1Touch && slope < 0){
               isStoSmallMainTouched = true;
            }
            else
            if(isBelowS1Touch && slope > 0){
               isStoSmallMainTouched = true;
            }
            
         }
         
         if(!isIntersect)
         {
            if(intersect(pS1,qS1,pS2,qS2)){
               double iAt =  intersectAt(pS1,qS1,pS2,qS2).y;
               
               order.lastStoIntersectSmall = iAt;
               
               if(isArea(+1, iAt, stoS_LineHigh) || isArea(-1, iAt, stoS_LineLow)){
                  isIntersect = true;
               }
            }
         }
         
         if(isStoSmallMainTouched && isIntersect){
            return true;
         }
         
      }
      else
      {
         return false;
      }
      
   }
   
   return false;
}


//####################################################################
//CCI
//####################################################################



int checkCCIDirection(int barAt, int sizeBar,InfoOrder& order){
   bool isTouched = false;
   int direction = 0;
   for( int j = barAt; j < sizeBar; j++){
      const int i = j;
      FPoint pS1;
      FPoint qS1;

      pS1.x = i;
      pS1.y = getCCI(i,order);
      qS1.x = i+1;
      qS1.y = getCCI(i+1,order);
      
      if(( isArea(+1, pS1.y, cci_LineHigh) || isArea(+1, qS1.y, cci_LineHigh) ) ||( isArea(-1, pS1.y, cci_LineLow) || isArea(-1, qS1.y, cci_LineLow) ))
      {
      
         bool isAboveS1Touch = isTouch(-1, pS1.y, qS1.y, cci_LineHigh);
         bool isBelowS1Touch = isTouch(+1, pS1.y, qS1.y, cci_LineLow);
         if(!isTouched && ( isAboveS1Touch || isBelowS1Touch )){
            
            double slope = getSlope(pS1,qS1);
            
            //OutText(StringFormat("last Time %f",slope),20);
            
            if(isAboveS1Touch && slope < 0){
               isTouched = true;
               direction = -1;
               
               order.lastCCI =  pS1.y;
               break; 
            }
            else
            if(isBelowS1Touch && slope > 0){
               isTouched = true;
               direction = 1;
               
               order.lastCCI =  pS1.y;
               break;
            }
            
         }
         
         
          break; // Break for calculate one times.
          
      }
      else
      {
         break; // Break for calculate one times.
      }
         
   }
      
   return direction;
   
}

double getCCI(int barAt,InfoOrder& order){
   return getCCI( cci_period,barAt,order);
}

double getCCI(int period,int barAt,InfoOrder& order){
   int timeframe = 0; // 0 means the current chart timeframe.
   
   return iCCI(order.symblo,timeframe,period,PRICE_TYPICAL,barAt);
}

//####################################################################
//BB
//####################################################################


/*
double getPercent(double minValue, double maxValue, double currentValue){
   
   
   
   
   return direction;
}
//*/
double getRealPIP(InfoOrder& order){
   return (1.0f / MathPow(10,Digits - Digits%2));
}

double getRealPIPSpan(InfoOrder& order){
   return (bb_pipNeary / MathPow(10,Digits - Digits%2));
}

double getDistance(double p1,double p2){
   return MathAbs(p1 - p2);
}

double getBBUpper(int barAt,InfoOrder& order){
   return getBB( bb_period,MODE_UPPER ,barAt,order);
}

double getBBMain(int barAt,InfoOrder& order){
   return getBB( bb_period,MODE_MAIN ,barAt,order);
}

double getBBLower(int barAt,InfoOrder& order){
   return getBB( bb_period,MODE_LOWER ,barAt,order);
}

double getBB(int period,int mode, int barAt,InfoOrder& order){
   int timeframe = 0; // 0 means the current chart timeframe.
   
   return iBands(order.symblo,timeframe,period,bb_deviation, 0, PRICE_CLOSE, mode,barAt);
}