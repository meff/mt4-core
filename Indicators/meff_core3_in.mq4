//+------------------------------------------------------------------+
//|                                                meff_core3_in.mq4 |
//|                                                        Sirisak.J |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Sirisak.J"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 5
#property indicator_plots   5
//--- plot order
#property indicator_label1  "order"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrYellow
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot tp
#property indicator_label2  "tp"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrGreen
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot sp
#property indicator_label3  "sl"
#property indicator_type3   DRAW_ARROW
#property indicator_color3  clrOrange
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
//--- plot sp
#property indicator_label5  "slOk"
#property indicator_type5   DRAW_ARROW
#property indicator_color5  clrRed
#property indicator_style5  STYLE_SOLID
#property indicator_width5  1
//--- plot tpOk
#property indicator_label4  "tpOk"
#property indicator_type4   DRAW_ARROW
#property indicator_color4  clrBlue
#property indicator_style4  STYLE_SOLID
#property indicator_width4  1
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
#include <meff_struct.mqh>
#include <meff_helper.mqh>

double         orderBuffer[];
double         tpBuffer[];
double         tpOkBuffer[];
double         slBuffer[];
double         slOkBuffer[];

string symbol;
int timeframe = 0;
int pricefield = 1;
datetime orderTime;
datetime tpTime;
bool tpAuto = false;
bool tpForceRun = false;
bool orderFinish = true;
double tpPrice;
double slPrice;
double pointHigh;
double orderPrice;
double directTP;
double directSL;

long tpCount = 0;
long slCount = 0;
long sl_sCount = 0;

long tpPoint = 0;
long slPoint = 0;

enum DirectionEnum {
   UP,
   DOWN,
   MID
};

void setBufferDefault (double &array[],int i) {
   if(array[i] > 20000000) {
      array[i] = -1;
   }
}

DirectionEnum orderDirect;

struct StochasticResultModel {
   bool isIntersected;
   DirectionEnum direction;
};

struct EnvResultModel {
   bool isIntersected;
   DirectionEnum direction;
};

struct ResultModel {
   bool isIntersected;
   DirectionEnum direction;
};

const int ID_ORDER = 0;
const int ID_TP = 1;
const int ID_SL = 2;
const int ID_TPK = 3;
const int ID_SLK = 4;

int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(ID_ORDER,orderBuffer);
   SetIndexBuffer(ID_TP,tpBuffer);
   SetIndexBuffer(ID_SL,slBuffer);
   SetIndexBuffer(ID_TPK,tpOkBuffer);
   SetIndexBuffer(ID_SLK,slOkBuffer);
   symbol = Symbol();
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   setBufferDefault(orderBuffer, 0 );
   setBufferDefault(tpBuffer, 0 );
   setBufferDefault(slBuffer, 0 );
   setBufferDefault(tpOkBuffer, 0 );
   setBufferDefault(slOkBuffer, 0 );


   
   ResultModel stoResult = checkFisher_MixIntersect(0);
   EnvResultModel envResult = checkEnvCorrect(0);
   
   DirectionEnum cciResult = checkCCI(1);
   double curPrice = (Bid + Ask) / 2.0f;
   if(stoResult.isIntersected && orderTime != time[0] && orderFinish && (cciResult == stoResult.direction || true) && (checkSAR(0) == stoResult.direction || true)){
      double pointTP = 500.0f / MathPow(10,Digits);
      double pointSL = 1000.0f / MathPow(10,Digits);
      if(!orderFinish) {
         slOkBuffer[0] = slPrice;
      }
      
      /*
      orderFinish = false;
      directTP = envResult.direction == UP ? 1 : -1;
      directSL = directTP * -1;
      orderTime = time[0];
      tpTime = time[0];
      pointHigh = 0;
      tpForceRun = false;
      orderDirect = envResult.direction;
      orderBuffer[0] = curPrice;
      orderPrice = orderBuffer[0];
      //tpBuffer[0] = orderBuffer[0] + (pointTP * directTP); 
      slPrice = slBuffer[0] = orderBuffer[0] + (pointSL * directSL);
      tpPrice = orderBuffer[0] + (pointTP * directTP); 
      tpAuto = true;
      //*/
      //*
      orderFinish = false;
      directTP = stoResult.direction == UP ? 1 : -1;
      directSL = directTP * -1;
      orderTime = time[0];
      tpTime = time[0];
      pointHigh = 0;
      tpForceRun = false;
      orderDirect = stoResult.direction;
      orderBuffer[0] = curPrice;
      orderPrice = orderBuffer[0];
      //tpBuffer[0] = orderBuffer[0] + (pointTP * directTP); 
      slPrice = slBuffer[0] = orderBuffer[0] + (pointSL * directSL);
      tpPrice = orderBuffer[0] + (pointTP * directTP); 
      tpAuto = true;
      //*/
   }
   
   double curPoint = MathAbs(orderPrice - curPrice);
   bool canTp = false;
   int minPf = -40;
   
   bool forceClose = false;
   
   int curTime = (TimeCurrent());
   int ordTime = (orderTime);
   int period = Period();
   int countCandle = (curTime - ordTime) / ( period * 60);
   
   if( countCandle > 0 && checkFisher_MixClose(orderDirect, 0)){
      //forceClose = true;
      tpForceRun = true;
      tpAuto = true;
   }
   
   /*
   if( countCandle < 7 ) {
      minPf = -60;
   }
   else {
      minPf = -25;
   }
   //*/
   
   //Print("TP: ", tpCount,", SL: ", slCount, ", SLs: ",sl_sCount, ", Point: ", tpPoint,"/",slPoint, " : ");
   
   if(!orderFinish && forceClose){
      
      //minPf = 0;
      if(!orderFinish && tpAuto && ((orderDirect == UP && curPrice <= orderPrice) || (orderDirect == DOWN && curPrice >= orderPrice ))){
         slOkBuffer[0] = curPrice;
         slBuffer[0] = -1;
         slCount++;
         tpForceRun = false;
         tpAuto = false;
         orderFinish = true;
         
         slPoint += curPoint * MathPow(10,Digits);
         return rates_total;
      }
      else if(false){
         tpOkBuffer[0] = curPrice;
         tpCount++;
         tpPoint += curPoint * MathPow(10,Digits);
         tpForceRun = false;
         tpAuto = false;
         tpBuffer[0] = -1;
         orderFinish = true;
         
         return rates_total;
      }

   }
   
   
   
   
   if(!orderFinish && tpAuto && ((orderDirect == UP && curPrice <= slPrice) || (orderDirect == DOWN && curPrice >= slPrice ))){
      slOkBuffer[0] = slPrice;
      slBuffer[0] = -1;
      slCount++;
      tpForceRun = false;
      tpAuto = false;
      orderFinish = true;
      
      slPoint += curPoint * MathPow(10,Digits);
      return rates_total;
   }
   
   if(!orderFinish && tpAuto && ((orderDirect == UP && curPrice >= tpPrice) || (orderDirect == DOWN && curPrice <= tpPrice ) || tpForceRun)){
      
      tpForceRun = true;
      if(tpTime != time[0]) {
         tpTime = time[0];
         tpBuffer[1] = -1;
      }

      if((orderDirect == UP && curPrice > orderPrice) || (orderDirect == DOWN && curPrice < orderPrice )){
         tpBuffer[0] = curPrice;   
         canTp = true;
      }
      else 
      {
         tpBuffer[0] = -1;
      }
      
      if(curPoint > pointHigh){
         pointHigh = curPoint;
      }
      
      EnvResultModel endEnv = checkEnvIntersect(0);
      
      if( canTp && endEnv.direction != MID && (endEnv.direction != orderDirect)){ 
         tpOkBuffer[0] = curPrice;
            tpCount++;
            tpPoint += curPoint * MathPow(10,Digits);
            
            tpForceRun = false;
         tpAuto = false;
         tpBuffer[0] = -1;
         orderFinish = true;
      }
       //Print("ddd ", (pointHigh-curPoint) * MathPow(10,Digits));
      /*
      if( (curPoint - pointHigh) * MathPow(10,Digits) < minPf ){
      
         if(canTp){
            tpOkBuffer[0] = curPrice;
            tpCount++;
            tpPoint += curPoint * MathPow(10,Digits);
            
         }
         else {
            slOkBuffer[0] = curPrice;
            slBuffer[0] = -1;
            sl_sCount++;
            slPoint += curPoint * MathPow(10,Digits);
         }
         
         tpForceRun = false;
         tpAuto = false;
         tpBuffer[0] = -1;
         orderFinish = true;
      }
      //*/
   }
   
   if(prev_calculated != 0){
      //orderBuffer[0] = prev_calculated;
   }
//--- return value of prev_calculated for next call
   return rates_total;
  }
  
ResultModel checkFisher_MixIntersect(int barAt) {
   ResultModel result;
   result.direction = MID;
   result.isIntersected = false;
   //34,11,6
   
   double sto = iStochastic(symbol,timeframe,6,3,3,MODE_EMA,pricefield, MODE_MAIN, barAt + 1);
   double cci = iCCI(symbol,timeframe,20,PRICE_TYPICAL,barAt + 1);
   
   double value0 = iCustom(symbol,timeframe,"Fisher",10,0,barAt);
   double value1 = iCustom(symbol,timeframe,"Fisher",10,0,barAt+1);
   double value2 = iCustom(symbol,timeframe,"Fisher",10,0,barAt+2);
   
   double value10 = iCustom(symbol,timeframe,"Fisher",10,0,barAt);
   double value11 = iCustom(symbol,timeframe,"Fisher",10,1,barAt);
   double value12 = iCustom(symbol,timeframe,"Fisher",10,2,barAt);
   
   
   //Print(value0,", ", sto,", ",cci);
   
   if(value0 > 0.2 && (true || sto < 50 && cci < -75)){
      result.direction = UP;
      result.isIntersected = true;
      
   }
   else if(value0 < -0.2 && (true ||sto > 50 && cci > 75)){
      result.direction = DOWN;
      result.isIntersected = true;
      
   }
  
   return result;
}

bool checkFisher_MixClose(DirectionEnum direction, int barAt) {
  
   double value0 = iCustom(symbol,timeframe,"Fisher",10,0,barAt);
   double value1 = iCustom(symbol,timeframe,"Fisher",10,0,barAt+1);

   
   
   if(value0 > 0.2 && value1 > 0 && direction == DOWN){
      return true;
      
   }
   else if(value0 < -0.2 && value1 < 0 && direction == UP){
   
      return true;
   }
  
   return false;
}
  
//+------------------------------------------------------------------+
ResultModel checkMA2_MixIntersect(int barAt) {
   ResultModel result;
   result.direction = MID;
   result.isIntersected = false;
   //34,11,6
   double maXLH0 = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_HIGH,barAt);
   double maXLL0 = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_LOW,barAt);
   
   
   if(isUP(barAt) && maXLH0 < Close[barAt] && isBarHLCheck(barAt,1, maXLH0) && validateMA2Up(barAt, 4, 2)) {
      result.direction = UP;
      result.isIntersected = true;
   } else if(isDown(barAt) && maXLL0 > Close[barAt] && isBarHLCheck(barAt,1, maXLL0) && validateMA2Down(barAt, 4, 2)) {
   
//      Print("Debug",isDown(barAt),maXLL0 > Close[barAt], isBarHLCheck(barAt,2, maXLL0))
      result.direction = DOWN;
      result.isIntersected = true;
   }
   return result;
}

bool validateMA2Down(int barAt,int len, int countMatch) {
   double value = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_HIGH,barAt);
   int match = 0;
   for(int i=0;i<len;i++){
      if(High[i+barAt] > value){
         match++;
      }
   }
   return match >= countMatch;
}

bool validateMA2Up(int barAt,int len, int countMatch) {
   double value = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_LOW,barAt);
   int match = 0;
   for(int i=0;i<len;i++){
      if(Low[i+barAt] < value){
         match++;
      }
   }
   return match >= countMatch;
}

bool isUP(int barAt){
   return Open[barAt] < Close[barAt];
}

bool isDown(int barAt){
   return Open[barAt] > Close[barAt];
}

bool isBarHLCheck(int barAt,int size, double value){
   for(int i=0;i<size;i++){
      if(High[i+barAt] > value && value > Low[i+barAt]){
         return true;
      }
   }
   
   return false;
}

ResultModel checkMA_MixIntersect(int barAt) {
   ResultModel result;
   result.direction = MID;
   result.isIntersected = false;
   //34,11,6
   double maL0 = iMA(symbol,timeframe,34,0,MODE_EMA,PRICE_CLOSE,barAt);
   double maM0 = iMA(symbol,timeframe,11,0,MODE_EMA,PRICE_CLOSE,barAt);
   double maS0 = iMA(symbol,timeframe,6,0,MODE_EMA,PRICE_CLOSE,barAt);
   
   double maL1 = iMA(symbol,timeframe,34,0,MODE_EMA,PRICE_CLOSE,barAt+1);
   double maM1 = iMA(symbol,timeframe,11,0,MODE_EMA,PRICE_CLOSE,barAt+1);
   double maS1 = iMA(symbol,timeframe,6,0,MODE_EMA,PRICE_CLOSE,barAt+1);
   
   if(maS0 > maM0 && maS0 > maL0 && maS1 < maL1) {
      result.isIntersected = true;
      result.direction = UP;
   }
   else if(maS0 < maM0 && maS0 < maL0 && maS1 > maL1) {
      result.isIntersected = true;
      result.direction = DOWN;
   }
   
   return result;
}

bool checkMA2_MixClose (int barAt) {

   double maXLH0 = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_HIGH,barAt);
   double maXLL0 = iMA(symbol,timeframe,20,0,MODE_EMA,PRICE_LOW,barAt);
   
   
   if(isBarHLCheck(barAt,1, maXLH0)) {
      return true;
   } else if(isBarHLCheck(barAt,1, maXLL0)) {
      return true;
   }
   return false;
}

bool checkMA_MixClose(int barAt) {
   ResultModel result;
   result.direction = MID;
   //34,11,6
   double maL0 = iMA(symbol,timeframe,34,0,MODE_EMA,PRICE_CLOSE,barAt);
   double maM0 = iMA(symbol,timeframe,11,0,MODE_EMA,PRICE_CLOSE,barAt);
   double maS0 = iMA(symbol,timeframe,6,0,MODE_EMA,PRICE_CLOSE,barAt);
   
 
   const int i = barAt;
   FPoint pS1;
   FPoint qS1;
   FPoint pS2;
   FPoint qS2;
   
  
   pS1.x = i;
   pS1.y = iMA(symbol,timeframe,6,0,MODE_EMA,PRICE_CLOSE, i);
   qS1.x = i+1;
   qS1.y = iMA(symbol,timeframe,6,0,MODE_EMA,PRICE_CLOSE, i + 1);
   
   pS2.x = i;
   pS2.y = iMA(symbol,timeframe,11,0,MODE_EMA,PRICE_CLOSE, i);
   qS2.x = i+1;
   qS2.y = iMA(symbol,timeframe,11,0,MODE_EMA,PRICE_CLOSE, i + 1);
   

   return intersect(pS1,qS1,pS2,qS2);
}

   
DirectionEnum checkSAR(int barAt){
   double value = iSAR(symbol,timeframe, 0.02f, 0.2f, barAt);
   
   return value > Open[barAt] ? DOWN : UP;
}

DirectionEnum checkCCI(int barAt){

   double value = iCCI(symbol,timeframe,30,PRICE_TYPICAL,barAt);
   
   if (value >= 100) return DOWN;
   if (value <= -100) return UP;
   return MID;
}

EnvResultModel checkEnvCorrect(int barAt) {
   EnvResultModel result;
   result.isIntersected = false;
   double envUpper = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_UPPER, barAt);
   double envLower = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_LOWER, barAt);
   
   double high = High[barAt];
   double low = Low[barAt];
   
   StochasticResultModel stoResult = checkEnvStoBefore(barAt, 5);
   
   if(isTouch(1,high,low,envUpper) && stoResult.isIntersected && stoResult.direction == DOWN){
      result.isIntersected = true;
      result.direction = DOWN;
   }
   else if(isTouch(1,high,low,envLower) && stoResult.isIntersected && stoResult.direction == UP){
      result.isIntersected = true;
      result.direction = UP;
   }

   return result;
}

ResultModel checkMAIntersect(int barAt) {
   ResultModel result;
   result.direction = MID;
   result.isIntersected = false;
   
   double ma = iMA(symbol,timeframe,2,0,MODE_EMA,PRICE_CLOSE,barAt);
   double envUpper = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_UPPER, barAt);
   double envLower = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_LOWER, barAt);
   
   const int i = barAt;
   FPoint pS1;
   FPoint qS1;
   FPoint pS2;
   FPoint qS2;
   
   FPoint pMa;
   FPoint qMa;
   //FPoint pLarge = {(double)i, getStochasticLarge(true,i)};      
   //FPoint qLarge = {(double)(i+1), getStochasticLarge(false,i)};
   
   pS1.x = i;
   pS1.y = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_UPPER, i);
   qS1.x = i+1;
   qS1.y = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_UPPER, i + 1);
   
   pS2.x = i;
   pS2.y = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_LOWER, i);
   qS2.x = i+1;
   qS2.y = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_LOWER, i + 1);
   
   pMa.x = i;
   pMa.y = iMA(symbol,timeframe,2,0,MODE_EMA,PRICE_CLOSE, i);
   qMa.x = i+1;
   qMa.y = iMA(symbol,timeframe,2,0,MODE_EMA,PRICE_CLOSE, i + 1);
   
   
   if(intersect(pS1,qS1,pMa,qMa) && pS1.y > pMa.y && qS1.y < qMa.y){
      result.direction = DOWN;
      result.isIntersected = true;
   }
   else if(intersect(pS2,qS2,pMa,qMa) && pS2.y < pMa.y && qS2.y > qMa.y){
      result.direction = UP;
      result.isIntersected = true;
   }
      
   return result;
}



EnvResultModel checkEnvIntersect(int barAt) {
   EnvResultModel result;
   result.direction = MID;
   result.isIntersected = false;
   double envUpper = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_UPPER, barAt);
   double envLower = iEnvelopes(symbol,timeframe,14, MODE_SMA, 0,PRICE_CLOSE,0.10f, MODE_LOWER, barAt);
      
   double high = High[barAt];
   double low = Low[barAt];
   
   if(isTouch(1,high,low,envUpper) ){
      result.isIntersected = true;
      result.direction = DOWN;
   }
   else if(isTouch(1,high,low,envLower)){
      result.isIntersected = true;
      result.direction = UP;
   }
   return result;
}

StochasticResultModel checkEnvStoBefore(int barAt, int count){
   StochasticResultModel result;
   result.isIntersected = false;
   
   for(int i = barAt; i < barAt+count; i++){
      StochasticResultModel result = checkStochasticCorrect(i);
      if(result.isIntersected){
         return result;
      }
   }
   
   return result;
}

const double K = 14;
   const double D = 3;
   const double S = 3;
   
StochasticResultModel checkStochasticCorrect(int barAt) {
   
   int sizeBar = Bars - 1;
   StochasticResultModel result;
   result.isIntersected = false;
   
   //for( int j = barAt; j < sizeBar; j++){
      const int i = barAt;
      FPoint pS1;
      FPoint qS1;
      FPoint pS2;
      FPoint qS2;
      //FPoint pLarge = {(double)i, getStochasticLarge(true,i)};      
      //FPoint qLarge = {(double)(i+1), getStochasticLarge(false,i)};
      
      pS1.x = i;
      pS1.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_MAIN, i);
      qS1.x = i+1;
      qS1.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_MAIN, i + 1);
      
      pS2.x = i;
      pS2.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_SIGNAL, i);
      qS2.x = i+1;
      qS2.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_SIGNAL, i + 1);
      
      double y0 = iStochastic(symbol,timeframe,5,3,3,MODE_EMA,pricefield, MODE_MAIN, 0);
      double y1 = iStochastic(symbol,timeframe,5,3,3,MODE_EMA,pricefield, MODE_SIGNAL, 0);
      
      if(intersect(pS1,qS1,pS2,qS2)){
         FPoint iAt = intersectAt(pS1,qS1,pS2,qS2);
         /*
         int maxValid1 = 8;
         for(int j=1;j<maxValid1;j++){
            double valid1 = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_MAIN, i + j);
            if( valid1 < 80 && valid1 > 20) {
               break;
            }
            
            if(j>=maxValid1-1){
               return result;
            }
         }
         //*/
         
         if( iAt.y > 80 )
         {
            result.isIntersected = true;
            result.direction = DOWN;
         }
         else if( iAt.y < 20 ) {
            result.isIntersected = true;
            result.direction = UP;
         }
         
         //break;
      }
   //}
   return result;
}


bool checkStochasticCloseOrder(int barAt){
      const int i = barAt;
      FPoint pS1;
      FPoint qS1;
      FPoint pS2;
      FPoint qS2;
      //FPoint pLarge = {(double)i, getStochasticLarge(true,i)};      
      //FPoint qLarge = {(double)(i+1), getStochasticLarge(false,i)};
      
      pS1.x = i;
      pS1.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_MAIN, i);
      qS1.x = i+1;
      qS1.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_MAIN, i + 1);
      
      pS2.x = i;
      pS2.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_SIGNAL, i);
      qS2.x = i+1;
      qS2.y = iStochastic(symbol,timeframe,K,D,S,MODE_EMA,pricefield, MODE_SIGNAL, i + 1);
      
      if(intersect(pS1,qS1,pS2,qS2)){ 
         return true;
      }
      return false;
}