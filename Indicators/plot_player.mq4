//+------------------------------------------------------------------+
//|                                                  plot_player.mq4 |
//|                                                        Sirisak.J |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Sirisak.J"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 1
#property indicator_plots   1
//--- plot order
const int ID_PLOT = 0;
#property indicator_label1  "plot"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrYellow
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
double plotBuffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(ID_PLOT,plotBuffer);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   setBufferDefault(plotBuffer, 0 );
   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

void setBufferDefault (double &array[],int i) {
   if(array[i] > 20000000) {
      array[i] = -1;
   }
}