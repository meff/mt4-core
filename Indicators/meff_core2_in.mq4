//+------------------------------------------------------------------+
//|                                                meff_core2_in.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#include <meff_struct.mqh>
#include <meff_helper.mqh>
#include <meff_noti.mqh>
#include <meff_core2.mqh>

#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   3
//--- plot order
#property indicator_label1  "order"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrPink
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot tp
#property indicator_label2  "tp"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot sp
#property indicator_label3  "sp"
#property indicator_type3   DRAW_ARROW
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
//--- indicator buffers
double         orderBuffer[];
double         tpBuffer[];
double         spBuffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

InfoOrder infoOrder;
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,orderBuffer);
  //  SetIndexStyle (0,DRAW_LINE,STYLE_SOLID,2);
   SetIndexBuffer(1,tpBuffer);
   SetIndexBuffer(2,spBuffer);
   
   initInfoOrder(Symbol(), infoOrder);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
  
   OrderBill orderBill;
   executeCore2(infoOrder,orderBill);
   if(orderBill.isValid){
      orderBuffer[0] = orderBill.price;
      tpBuffer[0] = orderBill.takeprofit;
   }
//---
   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
//---
   
  }
//+------------------------------------------------------------------+
